# Learn Git
Git is the industry standard for a version control tool right now in the
software industry. If you work in the IT field, it is going to be impossible for
you to not use Git. But that's not why you should learn Git. You should use it
because Git is a versatile and powerful tool that you'll find new uses for
everyday. It will change the way you approach many problems you'll face as a
software developer to the point you'll start to wonder what your life would be
like if you didn't have Git.

That being said, a tool is only as good as at the person wielding it. So it
helps to understand the tool so that you can use it effectively. I believe
knowing when not to use and how not to use a tool is an important part of
gaining competency with any tool. Do not use git and git forges as replacement
for a cloud storage solution. Even in a software project.

# About this page.
This page is more of a reference than an actual guide. For detailed information
about anything mentioned here refer the official Git documentation.

This page is for you to find the information you need and find it fast, hence
everything is kept to the point and concise. 
# The Lingo
- **Version Controlling**: It is the act of actively keeping track of changes to
a particular file or group of files as snapshots in a repository over time.
- **Repository**: A place (think of it like a folder) where you put files (text
files, videos files, audio files etc.).
- **Project Repository**: A place where you put the source code to your software
project.
- **Git Repository**: A place where Git puts all the necessary information it
needs to keep track of the changes you make in your project repository. It is
the `.git` folder in the root of your project repository. The git repository is
in your project repository.
- **Local Repository**: A project repository that sits in your computer's
hard-drive. 
- **Remote Repository**: A project repository that sits somewhere else other
than your computer. You can *push* the changes you've made in your **local
repository** to the **remote repository** or *pull* the changes present in the
**remote repository** to your **local repository**. Remote repositories often
work like cloud storage services like Google Drive or Nextcloud. Remote
repositories help us share our code with others and collaborate.
- **A Commit**: We mentioned that version controlling is the act of keep track
of changes to files, how we do it is by taking snapshots of the changes at a
point in time and storing it. This snapshot is called **a commit** and the act
of making this snapshot is often referred to as *'making a commit'*. Who decides
when to take a snapshot/make a commit? We developers do. So essentially from the
moment our project repository was an empty file, to when we added our first file
to when we have more files than we care to keep track of every addition and
removal from the repository is seen as a change and snapshots of the change is
recorded by the developer.
# Getting started with Git
- Follow the instructions on this [page](https://git-scm.com/book/en/v2/Getting-Started-First-Time-Git-Setup) 
  to set-up git.
- How to create a git repository?
  ```sh
  git init
  ```
  This will initialize a Git repository in your project repository.
- How to get the status of my get repository?
  ```sh
  git status
  ```
  This will show you all the files that's changed and that are not being tracked
  in your project repository. 
- How to create a branch and switch to it?
   ```sh
  git checkout -b branch-name
  ```
  If you want to branch from specific branch or commit add the tag or commit ID
  to the end. For example if you want to branch off from tag v1.0, your command 
  will look something like this.
  ```sh
  git checkout -b branch-name v1.0
  ```
- Alright you've made some changes and you want to commit your changes. So how to add/stage your changes?
  ```sh
  git add filepath1 filepath2
  ```
  When you want to stage everything all at once (not recommended):
  ```sh
  git add .
  ```
- How to commit your changes? 
  ```sh
  git commit
  ```
  This command will open a text editor in your terminal prompting you to write a
  commit message. Write the commit message, save the file and exit out of the
  editor. You have now made a commit. 
  
  A commit message has two parts: a title and a description. The title is a  one
  line summary of what the commit is about and the description provides a litle
  more information and context about the commit. Some commits can be  very
  straight forward and might not require a detailed description, in such cases
  the description can be omitted and can be done with the following command:
  ```sh
    git commit -m 'Commit message'
  ```
  
  It is a good idea to sign your commits with your GPG key. Here's the
  [documentation](https://docs.gitlab.com/ee/user/project/repository/gpg_signed_commits/#signing-commits-with-gpg)
  from Gitlab on how to create and use GPG keys with Git and Gitlab to sign your
  commits. This helps us be sure that the person who made the commit is infact
  you and not someone who broke into your Gitlab account. Signing commits is not
  mandatory for contributing but is recommended.
  - How to fix a mistake in your commit message after committing?  If it's the
  commit message of your last commit then it's pretty easy. Run the comment
  below and a text editor with your last commit message will open. Make the
  necessary changes, save and exit out of the editor. That's it.
  ```sh
  git commit --amend
  ```
  If the commit message you want to change is a few commits
  back you'll have to use a feature of git called `rebase`, which is a bit more
  advanced.  # Dealing with remote repositories There are multiple companies
  that offer git repository hosting as a service.  These websites are often
  called as 'forges' or 'git forges'. Github and Gitlab are the most popular
  ones. Between Github and Gitlab, I prefer Gitlab because apart from a few
  features aimed at project managers who are not programmers, most of Gitlab is
  actually free software and can be self-hosted while most of Github is
  proprietary. There are also other forges such as
  [Codeberg](https://codeberg.org/) and [Sourcehut](https://sourcehut.org/)
  which are both free software and can be self-hosted.

A remote git repository will have a url that will allow git to interact with it.
A typical Gitlab url looks like this
`https://gitlab.com/username/repo-namespace`. Here the `username` is your
username that you registered your account at the forge with and the
`repo-namespace` is the repository namespace that your initialized your
repository on the forge. You can use this link url to push to and pull from and
authentication is usually done using your username and password on the forge in
which your account is registered at. The issue with the username-password method
of authentication is that it's being phased out by some forges and if you have
accounts on multiple forges remembering your password on each one of them does
will not be easy. So I would recommend everyone to SSH to interact with remote
git repositories.  Here's the [documentation](https://docs.gitlab.com/ee/ssh/)
from Gitlab on how to create and use `ssh` with Git and Gitlab. The things
mentioned in this wiki page and the SSH key you generate here will work with
other forges as well.

- How to add the link to a remote repository to your local repo so as to push to
or pull from the remote repository? 
```sh 
git remote add origin git@gitlab.com:<your-username>/<repo-namespace>
```
Here origin is the name of the remote you're adding. If you'd like to push to
multiple repositories, you can add links to them with the same commit just use a
unique name for each remote URL.

- How to `push` code to your remote repository?
```sh
git push
```
  - When the remote is not the one which you set as upstream and not the main repository.
  ```sh
  git push <remote-name> <repo-name>
  ```

- How to `pull` code from your remote repository?
```sh
git pull
```
  - When the remote is not the one which you set as upstream and not the main repository.
  ```sh
  git pull <remote-name> <repo-name>
  ```
# Simple guide to contributing to projects.
- Fork the repo you wish to contribute to. 
- Clone your forked repo to your local machine. 
- Next, create a branch to work on.
- Now you can start working on making the changes you intended to make.
- Once done, you can add/stage your changes.
- Now you must commit your changes.
- Now that you've made a commit, it's time to push your changes.
- Once you've pushed your changes to your fork, start a merge request (MR)
against the main branch of the original repo. Here's the
[documentation](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
from Gitlab on how to create merge requests.

It's a good idea to tag and request someone to review your merge request.

Once reviewed the changes will be merged to the `master` branch.

# Further learning
- [Rebase](https://git-rebase.io/) in Git. I highly recommend that you learn how to properly use rebase. It can save you a lot of time.
- [Git + Email = <3](https://git-send-email.io/). Git is actually intended to be used with email. Take a look at this [video](https://spacepub.space/w/3JhBcvEYbminv8ji4k84gx) to see how efficient this is in comparison to web based systems. While you're at it take a look at why [plain text emails](https://useplaintext.email/) are better too.
