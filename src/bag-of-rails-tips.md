# A bag of rails tips

At the time of me writing this I am a junior full stack developer who works with
Rails and React.  Rails is a wonderful framework that is more often than not, a
joy to work with. Here are a few things I think every beginner working in rails
should know, in no particular order. Before I begin, I'd like to make it clear
that these are tips even though some of them may feel like hard rules or
conventions that are to be followed in every use case and scenario, let me
assure you they're nothing of the sort. Use them at your own discretion.

- [Byebug](https://github.com/deivid-rodriguez/byebug) and
  [Pry](https://github.com/pry/pry), they're your best friends. Get to know them
  from day one. Thank me later.
- Really good methods are not more than a single line long and often don't take
  any parameters.
- If an object/method takes too long to read and understand, then a refactor is
  in order.
- Replace temporary variables with queries.
  ([What?](https://refactoring.com/catalog/replaceTempWithQuery.html) and
  [Why?](https://refactoring.guru/replace-temp-with-query))
- Refactor early on, the more you wait to refactor the cost of it also
  increases. It is **always** a good idea to refactor code to make it more
  readable.
- When your language's/framework's community has already figured a solution to a
  common problem, do not try and reinvent the wheel. (Unless you **really**
  think there's something **seriously** wrong with the solution.). Rails has got
  you covered in many aspects of building a web app. Get to know your framework.
  Do it the rails way!
- Even though it does not say so in the framework's tag line, Rails is great
  choice for perfectionists. This is also my way of telling you to aim for
  perfection.
- Put a lot of thought into the database design, then start building. 
  - Make sure your schema doesn't leave room for ambiguity.
  - Make sure data is never duplicated within the schema.
  - The schema should pretty much reflect the business logic. All
    implicit and explicit assertions in the business logic should be reflected
    in the schema as well.
- When your models start to get big start assessing your code, a refactor is in
  order. Of course any web app that you make will have one or two big models.
  These models are usually what the app is about. If you're making a blog
  app the models are usually going to be post and author/user models. These
  models are allowed to be a little big than the rest but even then be very
  careful. If a specific functionality is what is making the model bloat maybe
  you need to put it in a
  [service](https://www.toptal.com/ruby-on-rails/rails-service-objects-tutorial)
  or maybe you're trying to squash together, what should be two models into one.
- On the other had if one model is constantly asking questions about another
  model to perform it's functionalities, asses your design again. ([Read
  more](https://www.martinfowler.com/bliki/TellDontAsk.html))
- Always use RESTful routes. Keep your controllers small. Methods related
  to functionality goes in the model or a service, **not the controller**.
- Do not be afraid to make simple ruby objects. Remember we are doing OOP, data
  and functionality stays together.
- Trust your instincts. If you feel something's not right, do not be afraid to
  probe further into whatever's making you uneasy.
- Have a mentor, you can learn programming without one but having one just makes
  everything way more easier.
